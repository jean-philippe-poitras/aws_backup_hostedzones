#!/usr/bin/env bash

get_recordset_pattern="aws route53 list-resource-record-sets --hosted-zone-id %s --query \"ResourceRecordSets[?Name == '%s.' && Type == 'A']\""

current_date=$(date +%Y-%m-%d.%H-%M-%S)
backup_directory="backup_${current_date}"

mkdir "${backup_directory}"

zone_filter="existing_zones/*.dat"
total_zones=$(ls -l $zone_filter|wc -l)
current_zone=1

for hosted_zone in $zone_filter; 
do
	basename_hosted_zone=$(basename $hosted_zone)
	hosted_zone_id="${basename_hosted_zone%%.*}";

	printf "\nBackup for zone %s [%d/%d]\n" "${hosted_zone_id}" "${current_zone}" "${total_zones}"	
	printf "==========================\n"

	total_records=$(wc -l $hosted_zone | cut -f1 -d' ')
	current_record=1

	while read record;
	do
		printf "Backuping %s...[%d/%d]\n" "${record}" "${current_record}" "${total_records}"
		cmd=$(printf "${get_recordset_pattern}\n" "${hosted_zone_id}" "${record}")
		#echo "$cmd"
		eval ${cmd} > "${backup_directory}/${hosted_zone_id}_${record}.json"
		((current_record++))
	done < $hosted_zone
	
	((current_zone++))
done

printf "\nBackup done\n"
printf "Backup folder is %s\n" "${backup_directory}"
